package com.konradkevin.socialshake;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.konradkevin.socialshake.adapters.ActiveInvitationsAdapter;
import com.konradkevin.socialshake.models.Invitation;
import com.konradkevin.socialshake.models.User;
import com.konradkevin.socialshake.providers.AuthenticationProvider;
import com.konradkevin.socialshake.providers.FirebaseProvider;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.konradkevin.socialshake.UserActivity.EXTRA_USER;

public class HomeFragment extends Fragment implements ActiveInvitationsAdapter.Listener, ValueEventListener {
    private List<Invitation> invitations;
    private ActiveInvitationsAdapter adapter;
    private FirebaseProvider firebaseProvider;
    private AuthenticationProvider authenticationProvider;

    @BindView(R.id.invitation) TextView numRequests;
    @BindView(R.id.totalRequest) TextView totalRequests;
    @BindView(R.id.requestsHomeRecyclerView) RecyclerView recyclerView;

    public HomeFragment() {
        invitations = new ArrayList<>();
        firebaseProvider = FirebaseProvider.getInstance();
        authenticationProvider = AuthenticationProvider.getInstance();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            PackageInfo info = getActivity().getPackageManager().getPackageInfo("com.konradkevin.socialshake", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.firebaseProvider.getInvitations().addValueEventListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        this.firebaseProvider.getInvitations().removeEventListener(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new ActiveInvitationsAdapter(invitations);
        adapter.setListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onInvitationAcceptClick(Invitation invitation) {
        this.firebaseProvider.updateInvitationMatch(invitation, 1).addOnFailureListener(Throwable::printStackTrace);
    }

    @Override
    public void onInvitationRefuseClick(Invitation invitation) {
        this.firebaseProvider.updateInvitationMatch(invitation, -1).addOnFailureListener(Throwable::printStackTrace);
    }

    @Override
    public void onInvitationClick(Invitation invitation) {
        User me = authenticationProvider.getCurrentUser();
        User other = me.getUid().equals(invitation.getFromUser().getUid()) ? invitation.getToUser() : invitation.getFromUser();

        Intent intent = new Intent(this.getContext(), UserActivity.class);
        intent.putExtra(EXTRA_USER, other);
        startActivity(intent);
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        invitations.clear();
        int total = 0;

        for (DataSnapshot invitationSnapshot: dataSnapshot.getChildren()) {
            Invitation invitation = invitationSnapshot.getValue(Invitation.class);

            if (invitation == null) {
                continue;
            }

            invitation.setId(invitationSnapshot.getKey());

            User user = authenticationProvider.getCurrentUser();
            if (invitation.getTo().equals(user.getUid()) || invitation.getFrom().equals(user.getUid())) {
                total++;

                firebaseProvider.getUser(invitation.getFrom()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        invitation.setFromUser(dataSnapshot.getValue(User.class));

                        firebaseProvider.getUser(invitation.getTo()).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                invitation.setToUser(dataSnapshot.getValue(User.class));

                                if (invitation.getToUser().getUid().equals(user.getUid()) && invitation.getMatchTo() == 0) {
                                    invitations.add(invitation);
                                    adapter.notifyDataSetChanged();
                                    numRequests.setText(String.valueOf(invitations.size()));
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) { }
                        });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {}
                });
            }
        }

        totalRequests.setText(String.valueOf(total));
        numRequests.setText(String.valueOf(invitations.size()));
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {}
}