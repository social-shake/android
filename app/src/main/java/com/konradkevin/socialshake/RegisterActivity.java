package com.konradkevin.socialshake;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.konradkevin.socialshake.models.User;
import com.konradkevin.socialshake.providers.AuthenticationProvider;
import com.konradkevin.socialshake.providers.FirebaseProvider;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.hoang8f.widget.FButton;

import static java.lang.String.valueOf;

public class RegisterActivity extends AppCompatActivity {
    private FirebaseProvider firebaseProvider;
    private AuthenticationProvider authenticationProvider;
    private FirebaseAuth auth;

    @BindView(R.id.email) EditText email;
    @BindView(R.id.password) EditText password;
    @BindView(R.id.name) EditText name;

    public RegisterActivity() {
        firebaseProvider = FirebaseProvider.getInstance();
        authenticationProvider = AuthenticationProvider.getInstance();
        auth = FirebaseAuth.getInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        FButton submit = findViewById(R.id.valid);
        submit.setOnClickListener(view -> register());

        FButton connect = findViewById(R.id.connect);
        connect.setOnClickListener(view -> goToLoginActivity());
    }

    public void register(){
        User user = new User();
        user.setEmail(valueOf(email.getText()));
        user.setName(valueOf(name.getText()));
        user.setUid(auth.getUid());

        firebaseProvider.createUser(valueOf(email.getText()), valueOf(password.getText()))
                .addOnFailureListener(Throwable::printStackTrace)
                .addOnSuccessListener(authResult -> {
                    user.setUid(authResult.getUser().getUid());
                    firebaseProvider.postUser(user)
                            .addOnSuccessListener(aVoid -> goToMainActivity())
                            .addOnFailureListener(Throwable::printStackTrace);
                });
    }

    public void goToLoginActivity(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void goToMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

}
