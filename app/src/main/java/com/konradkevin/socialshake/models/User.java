package com.konradkevin.socialshake.models;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
    private String uid;
    private String email;
    private String name;
    private String gloveId;
    private String deviceToken;
    private String facebookId;
    private String twitterId;
    private String linkedInId;
    private String instagramId;
    private String googleId;

    public User() {}

    public User(String uid, String email, String name, String gloveId, String deviceToken, String facebookId, String twitterId, String linkedInId, String instagramId, String googleId) {
        this.uid = uid;
        this.email = email;
        this.name = name;
        this.gloveId = gloveId;
        this.deviceToken = deviceToken;
        this.facebookId = facebookId;
        this.twitterId = twitterId;
        this.linkedInId = linkedInId;
        this.instagramId = instagramId;
        this.googleId = googleId;
    }

    protected User(Parcel in) {
        uid = in.readString();
        email = in.readString();
        name = in.readString();
        gloveId = in.readString();
        deviceToken = in.readString();
        facebookId = in.readString();
        twitterId = in.readString();
        linkedInId = in.readString();
        instagramId = in.readString();
        googleId = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(uid);
        parcel.writeString(email);
        parcel.writeString(name);
        parcel.writeString(gloveId);
        parcel.writeString(deviceToken);
        parcel.writeString(facebookId);
        parcel.writeString(twitterId);
        parcel.writeString(linkedInId);
        parcel.writeString(instagramId);
        parcel.writeString(googleId);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGloveId() {
        return gloveId;
    }

    public void setGloveId(String gloveId) {
        this.gloveId = gloveId;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getTwitterId() {
        return twitterId;
    }

    public void setTwitterId(String twitterId) {
        this.twitterId = twitterId;
    }

    public String getLinkedInId() {
        return linkedInId;
    }

    public void setLinkedInId(String linkedInId) {
        this.linkedInId = linkedInId;
    }

    public String getInstagramId() {
        return instagramId;
    }

    public void setInstagramId(String instagramId) {
        this.instagramId = instagramId;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }
}
