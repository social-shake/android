package com.konradkevin.socialshake.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Invitation implements Parcelable {
    private String id;
    private String from;
    private String to;
    private User fromUser;
    private User toUser;
    private String gloveIdFrom;
    private String gloveIdTo;
    private int matchFrom;
    private int matchTo;
    private long creationDate;

    public Invitation() {}

    protected Invitation(Parcel in) {
        id = in.readString();
        from = in.readString();
        to = in.readString();
        fromUser = in.readParcelable(User.class.getClassLoader());
        toUser = in.readParcelable(User.class.getClassLoader());
        gloveIdFrom = in.readString();
        gloveIdTo = in.readString();
        matchFrom = in.readInt();
        matchTo = in.readInt();
        creationDate = in.readLong();
    }

    public static final Creator<Invitation> CREATOR = new Creator<Invitation>() {
        @Override
        public Invitation createFromParcel(Parcel in) {
            return new Invitation(in);
        }

        @Override
        public Invitation[] newArray(int size) {
            return new Invitation[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(from);
        parcel.writeString(to);
        parcel.writeParcelable(fromUser, i);
        parcel.writeParcelable(toUser, i);
        parcel.writeString(gloveIdFrom);
        parcel.writeString(gloveIdTo);
        parcel.writeInt(matchFrom);
        parcel.writeInt(matchTo);
        parcel.writeLong(creationDate);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getGloveIdFrom() {
        return gloveIdFrom;
    }

    public void setGloveIdFrom(String gloveIdFrom) {
        this.gloveIdFrom = gloveIdFrom;
    }

    public String getGloveIdTo() {
        return gloveIdTo;
    }

    public void setGloveIdTo(String gloveIdTo) {
        this.gloveIdTo = gloveIdTo;
    }

    public int getMatchFrom() {
        return matchFrom;
    }

    public void setMatchFrom(int matchFrom) {
        this.matchFrom = matchFrom;
    }

    public int getMatchTo() {
        return matchTo;
    }

    public void setMatchTo(int matchTo) {
        this.matchTo = matchTo;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }


    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }
}
