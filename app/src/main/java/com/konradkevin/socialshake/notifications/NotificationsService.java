package com.konradkevin.socialshake.notifications;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.konradkevin.socialshake.MainActivity;
import com.konradkevin.socialshake.R;

import java.util.Map;

public class NotificationsService extends FirebaseMessagingService {
    private static final String TAG = "NotificationsService";

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "New token: " + token);
        sendRegistrationToServer(token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Map<String, String> data = remoteMessage.getData();

        if (data.size() > 0) {
            sendNotification(data);
        }
    }

    private void sendNotification(Map<String, String> messageData) {
        Log.d(TAG, messageData.toString());
        String channelId = getString(R.string.default_notification_channel_id);
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager == null) {
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.web_hi_res_512)
                        .setContentTitle("FCM Message")
                        .setAutoCancel(true)
                        .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                        .setDefaults(NotificationCompat.DEFAULT_ALL)
                        .setContentText(messageData.get("message"))
                        .setContentIntent(pendingIntent);

        notificationManager.notify(666, notificationBuilder.build());
    }

    private void sendRegistrationToServer(String token) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        while(user == null) {
            user = FirebaseAuth.getInstance().getCurrentUser();
        }

        String userId = user.getUid();
        FirebaseDatabase.getInstance().getReference().child("users/" + userId + "/deviceToken").setValue(token);
    }
}