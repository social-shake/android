package com.konradkevin.socialshake;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.konradkevin.socialshake.models.User;
import com.konradkevin.socialshake.providers.AuthenticationProvider;
import com.konradkevin.socialshake.providers.FirebaseProvider;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.hoang8f.widget.FButton;

public class SettingsFragment extends Fragment {
    private FirebaseProvider firebaseProvider;
    private AuthenticationProvider authenticationProvider;
    CallbackManager callbackManager;
    TwitterAuthClient mTwitterAuthClient;
    @BindView(R.id.glove_id) TextView id;
    @BindView(R.id.link_facebook_button) LoginButton loginButton;
    @BindView(R.id.link_google_button) FButton linkGo;
    @BindView(R.id.link_linkedin_button) FButton linkLdin;
    @BindView(R.id.link_instagram_button) FButton linkInsta;
    @BindView(R.id.link_twitter_button) FButton linkTwitter;
    @BindView(R.id.logout) FButton logout;
    private Dialog myDialog;

    public SettingsFragment() {
        firebaseProvider = FirebaseProvider.getInstance();
        authenticationProvider = AuthenticationProvider.getInstance();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       TwitterConfig config = new TwitterConfig.Builder(getActivity())
              .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig("MrDAPEe8jlM4tGDARTv8RMQnu", "BOirXWcJ0Srz3QttAnV67wsd47f03kkp8jQd5DYKcMXC9zJBAE"))
                .debug(true)
                .build();
        Twitter.initialize(config);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        myDialog = new Dialog(getActivity());
        myDialog.setContentView(R.layout.addusernamedialog);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        User user = authenticationProvider.getCurrentUser();
        id.setText(String.valueOf(user.getGloveId()));

        mTwitterAuthClient= new TwitterAuthClient();
        FacebookSdk.sdkInitialize(getActivity());

        callbackManager = CallbackManager.Factory.create();

        logout.setOnClickListener(this::signOut);

        loginButton.setReadPermissions("email");
        loginButton.setFragment(this);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String facebookId = loginResult.getAccessToken().getUserId();
                User user = authenticationProvider.getCurrentUser();
                user.setFacebookId(facebookId);
                firebaseProvider.getUser(user.getUid()).child("facebookId").setValue(user.getFacebookId());
            }

            @Override
            public void onCancel() {}

            @Override
            public void onError(FacebookException exception) {}
        });

        linkTwitter.setOnClickListener(v -> mTwitterAuthClient.authorize(getActivity(), new Callback<TwitterSession>() {

            @Override
            public void success(Result<TwitterSession> twitterSessionResult) {
               Log.d("mamamam","SUCCCESS");
                Log.d("mamamam",twitterSessionResult.data.getUserName());

            }
            @Override
            public void failure(TwitterException e) {
                e.printStackTrace();
                Log.d("TWITTER","azhkjeza"+e.getMessage());
            }
        }));

        linkInsta.setOnClickListener(v -> {
           ImageView LogoImg  = myDialog.findViewById(R.id.dialogLogo);
            EditText username = myDialog.findViewById(R.id.usernameInput);
           LogoImg.setImageDrawable(getResources().getDrawable(R.drawable.instagram));
           FButton submitUsrname = myDialog.findViewById(R.id.submitUName);
            myDialog.show();
            submitUsrname.setOnClickListener(v1 -> {
                if(!username.getText().toString().trim().equals("")){
                    FirebaseDatabase.getInstance().getReference().child("users").child(FirebaseAuth.getInstance().getUid()).child("instagramId").setValue(username.getText().toString());
                    myDialog.hide();
                }
            });
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
         mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void signOut(View view) {
        authenticationProvider.signOut();

        if (getActivity() != null) {
            Intent intent = new Intent(getContext(), LoginActivity.class);
            startActivity(intent);
            getActivity().finish();
        }
    }
}
