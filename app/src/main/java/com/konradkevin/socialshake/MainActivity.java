package com.konradkevin.socialshake;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcB;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;

import com.konradkevin.socialshake.models.User;
import com.konradkevin.socialshake.providers.AuthenticationProvider;
import com.konradkevin.socialshake.providers.FirebaseProvider;

import butterknife.BindView;
import butterknife.ButterKnife;

import static java.lang.String.valueOf;

public class MainActivity extends AppCompatActivity {
    private FirebaseProvider firebaseProvider;
    private AuthenticationProvider authenticationProvider;
    private final String[][] techList = new String[][] {
            new String[] {
                    NfcA.class.getName(),
                    NfcB.class.getName(),
                    NfcF.class.getName(),
                    NfcV.class.getName(),
                    IsoDep.class.getName(),
                    MifareClassic.class.getName(),
                    MifareUltralight.class.getName(), Ndef.class.getName()
            }
    };

    BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener = item -> {
        android.support.v4.app.Fragment selectedFragment;

        switch (item.getItemId()) {
            case R.id.navigation_home:
                selectedFragment =  new HomeFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.navFrame, selectedFragment).commit();
                break;
            case R.id.navigation_dashboard:
                selectedFragment =  new SettingsFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.navFrame, selectedFragment).commit();
                break;
            case R.id.navigation_notifications:
                selectedFragment =  new NotificationsFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.navFrame, selectedFragment).commit();
                break;
        }

        return true;
    };

    @BindView(R.id.navigation) BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        firebaseProvider = FirebaseProvider.getInstance();
        authenticationProvider = AuthenticationProvider.getInstance();

        if (authenticationProvider.getCurrentUser() == null){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
        bottomNavigationView.setSelectedItemId(R.id.navigation_home);
    }

    @Override
    public void onResume() {
        super.onResume();

        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        IntentFilter filter = new IntentFilter();
        filter.addAction(NfcAdapter.ACTION_TAG_DISCOVERED);
        filter.addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filter.addAction(NfcAdapter.ACTION_TECH_DISCOVERED);

        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter != null) {
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, new IntentFilter[]{filter}, this.techList);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter != null ) {
            nfcAdapter.disableForegroundDispatch(this);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (intent.getAction() == null || !intent.getAction().equals(NfcAdapter.ACTION_TAG_DISCOVERED)) {
            return;
        }

        User user = authenticationProvider.getCurrentUser();
        user.setGloveId(ByteArrayToHexString(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID)));
        firebaseProvider.postUser(user).addOnFailureListener(Throwable::printStackTrace);
    }

    private String ByteArrayToHexString(byte [] inArray) {
        int i, j, in;
        String [] hex = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
        StringBuilder outBuilder = new StringBuilder();

        for(j = 0 ; j < inArray.length ; ++j) {
            in = (int) inArray[j] & 0xff;
            i = (in >> 4) & 0x0f;
            outBuilder.append(hex[i]);
            i = in & 0x0f;
            outBuilder.append(hex[i]);
        }
        String out = outBuilder.toString();

        Long decimal = Long.parseLong(out, 16);
        out = valueOf(decimal);
        return out;
    }
}