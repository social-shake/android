package com.konradkevin.socialshake;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.konradkevin.socialshake.models.User;
import com.konradkevin.socialshake.providers.AuthenticationProvider;
import com.konradkevin.socialshake.providers.FirebaseProvider;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.hoang8f.widget.FButton;

public class LoginActivity extends AppCompatActivity {

    private FirebaseProvider firebaseProvider;
    private AuthenticationProvider authenticationProvider;

    @BindView(R.id.email) EditText email;
    @BindView(R.id.password) EditText password;

    public LoginActivity() {
        firebaseProvider = FirebaseProvider.getInstance();
        authenticationProvider = AuthenticationProvider.getInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        FButton submit = findViewById(R.id.valid);
        submit.setOnClickListener(view -> connect());

        FButton register = findViewById(R.id.register);
        register.setOnClickListener(view -> goToRegisterActivity());

    }

    @Override
    protected void onResume() {
        super.onResume();
        FirebaseUser firebaseUser = firebaseProvider.getCurrentFirebaseUser();
        if (firebaseUser != null) {
            createUserAndGoToHome(firebaseUser);
        }
    }

    public void connect(){
        firebaseProvider.signIn(email.getText().toString(), password.getText().toString())
                .addOnFailureListener(this::onSignInFailure)
                .addOnSuccessListener(this::onSignInSuccess);
    }

    public void goToRegisterActivity(){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private void onSignInSuccess(AuthResult authResult) {
        createUserAndGoToHome(authResult.getUser());
    }

    private void onSignInFailure(Exception exception) {
        exception.printStackTrace();
        authenticationProvider.signOut();
    }

    private void createUserAndGoToHome(FirebaseUser firebaseUser) {
        firebaseProvider.getUser(firebaseUser.getUid()).addValueEventListener(listener);
    }

    private void goToHomeActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private ValueEventListener listener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            User user = dataSnapshot.getValue(User.class);
            if (user == null) {
                return;
            }

            user.setUid(dataSnapshot.getKey());
            authenticationProvider.setCurrentUser(user);
            goToHomeActivity();
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {}
    };
}
