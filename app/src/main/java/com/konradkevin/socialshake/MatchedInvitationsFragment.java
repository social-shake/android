package com.konradkevin.socialshake;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.konradkevin.socialshake.adapters.MatchedInvitationsAdapter;
import com.konradkevin.socialshake.models.Invitation;
import com.konradkevin.socialshake.models.User;
import com.konradkevin.socialshake.providers.AuthenticationProvider;
import com.konradkevin.socialshake.providers.FirebaseProvider;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.konradkevin.socialshake.UserActivity.EXTRA_USER;

public class MatchedInvitationsFragment extends Fragment implements MatchedInvitationsAdapter.Listener, ValueEventListener {
    private List<Invitation> invitations;
    private MatchedInvitationsAdapter adapter;
    private FirebaseProvider firebaseProvider;
    private AuthenticationProvider authenticationProvider;

    @BindView(R.id.notificationsRecyclerView) RecyclerView recyclerView;

    public MatchedInvitationsFragment() {
        invitations = new ArrayList<>();
        firebaseProvider = FirebaseProvider.getInstance();
        authenticationProvider = AuthenticationProvider.getInstance();
    }

    @Override
    public void onResume() {
        super.onResume();
        this.firebaseProvider.getInvitations().addValueEventListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        this.firebaseProvider.getInvitations().removeEventListener(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new MatchedInvitationsAdapter(invitations);
        adapter.setListener(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onInvitationClick(Invitation invitation) {
        User me = authenticationProvider.getCurrentUser();
        User other = me.getUid().equals(invitation.getFromUser().getUid()) ? invitation.getToUser() : invitation.getFromUser();

        Intent intent = new Intent(this.getContext(), UserActivity.class);
        intent.putExtra(EXTRA_USER, other);
        startActivity(intent);
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        invitations.clear();

        for (DataSnapshot invitationSnapshot: dataSnapshot.getChildren()) {
            Invitation invitation = invitationSnapshot.getValue(Invitation.class);

            if (invitation == null) {
                continue;
            }

            invitation.setId(invitationSnapshot.getKey());

            User user = authenticationProvider.getCurrentUser();
            if (invitation.getTo().equals(user.getUid()) || invitation.getFrom().equals(user.getUid())) {

                firebaseProvider.getUser(invitation.getFrom()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        invitation.setFromUser(dataSnapshot.getValue(User.class));

                        firebaseProvider.getUser(invitation.getTo()).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                invitation.setToUser(dataSnapshot.getValue(User.class));

                                if ((invitation.getMatchFrom() == 1 && invitation.getMatchTo() == 1) && (invitation.getToUser().getUid().equals(user.getUid()) || invitation.getFromUser().getUid().equals(user.getUid()))) {
                                    invitations.add(invitation);
                                    adapter.notifyDataSetChanged();
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) { }
                        });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {}
                });
            }
        }
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {}
}
