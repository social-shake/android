package com.konradkevin.socialshake;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.konradkevin.socialshake.models.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.hoang8f.widget.FButton;

public class UserActivity extends AppCompatActivity {
    public static final String EXTRA_USER = "extra_user";
    private User user;

    @BindView(R.id.username) TextView username;
    @BindView(R.id.name) TextView name;
    @BindView(R.id.email) TextView email;
    @BindView(R.id.f_twitter_button) FButton twitterButton;
    @BindView(R.id.f_facebook_button) FButton facebookButton;
    @BindView(R.id.f_linkedin_button) FButton linkedInButton;
    @BindView(R.id.f_instagram_button) FButton instagramButton;
    @BindView(R.id.f_google_button) FButton googleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);

        if (getIntent().getExtras() != null) {
            user = getIntent().getParcelableExtra(EXTRA_USER);
        }

        username.setText(user.getName());
        name.setText(user.getName());
        email.setText(user.getEmail());

        twitterButton.setVisibility(user.getTwitterId() == null ? View.GONE : View.VISIBLE);
        facebookButton.setVisibility(user.getFacebookId() == null ? View.GONE : View.VISIBLE);
        linkedInButton.setVisibility(user.getLinkedInId() == null ? View.GONE : View.VISIBLE);
        instagramButton.setVisibility(user.getInstagramId() == null ? View.GONE : View.VISIBLE);
        googleButton.setVisibility(user.getGoogleId() == null ? View.GONE : View.VISIBLE);

        facebookButton.setOnClickListener(v -> goToFBUrl(user.getFacebookId()));
        instagramButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToInstaUrl(user.getInstagramId());
            }
        });
    }

    private void goToFBUrl (String id) {
        Uri uriUrl = Uri.parse("http://www.facebook.com/"+id);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    private void goToInstaUrl (String id) {
        Uri uriUrl = Uri.parse("http://www.instagram.com/"+id);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }
}
