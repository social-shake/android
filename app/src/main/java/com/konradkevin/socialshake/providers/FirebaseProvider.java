package com.konradkevin.socialshake.providers;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.konradkevin.socialshake.models.Invitation;
import com.konradkevin.socialshake.models.User;

public class FirebaseProvider {
    private static FirebaseProvider instance;
    private static final String INVITATIONS_REFERENCE = "invitations";
    private static final String NOTIFICATIONS_REFERENCE = "notifications";
    private static final String USERS_REFERENCE = "users";
    private FirebaseAuth firebaseAuth;
    private AuthenticationProvider authenticationProvider;
    private DatabaseReference dbRef;

    private FirebaseProvider() {
        this.firebaseAuth = FirebaseAuth.getInstance();
        this.authenticationProvider = AuthenticationProvider.getInstance();
        this.dbRef = FirebaseDatabase.getInstance().getReference();
    }

    public static FirebaseProvider getInstance() {
        if (instance == null) {
            instance = new FirebaseProvider();
        }

        return instance;
    }

    public FirebaseUser getCurrentFirebaseUser() {
        return firebaseAuth.getCurrentUser();
    }

    public Task<AuthResult> signIn(String email, String password) {
        return firebaseAuth.signInWithEmailAndPassword(email, password);
    }

    public Task<AuthResult> createUser(String email, String password) {
        return firebaseAuth.createUserWithEmailAndPassword(email, password);
    }

    public Task<Void> updateUser(AuthResult authResult, String username) {
        final FirebaseUser user = authResult.getUser();
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(username).build();
        return user.updateProfile(profileUpdates);
    }

    public DatabaseReference getUser(String id) {
        return dbRef.child(USERS_REFERENCE).child(id);
    }

    public Task<Void> postUser(User user) {
        DatabaseReference userRef = getUser(user.getUid());
        return userRef.setValue(user);
    }

    public DatabaseReference getInvitations() {
        return dbRef.child(INVITATIONS_REFERENCE);
    }

    public DatabaseReference getNotifications() {
        return dbRef.child(NOTIFICATIONS_REFERENCE);
    }

    public DatabaseReference getInvitation(Invitation invitation) {
        return getInvitations().child(invitation.getId());
    }

    public Task<Void> updateInvitationMatch(Invitation invitation, int match) {
        User user = authenticationProvider.getCurrentUser();
        String child = invitation.getToUser().getUid().equals(user.getUid()) ? "matchTo" : "matchFrom";
        return getInvitation(invitation).child(child).setValue(match);
    }
}
