package com.konradkevin.socialshake.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.konradkevin.socialshake.R;
import com.konradkevin.socialshake.models.Invitation;
import com.konradkevin.socialshake.models.User;
import com.konradkevin.socialshake.providers.AuthenticationProvider;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActiveInvitationsAdapter extends RecyclerView.Adapter<ActiveInvitationsAdapter.ViewHolder> {
    private List<Invitation> invitations;
    private AuthenticationProvider authenticationProvider;
    private Listener listener;

    public ActiveInvitationsAdapter(List<Invitation> invitations) {
        this.authenticationProvider = AuthenticationProvider.getInstance();
        this.invitations = invitations;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }


    @Override
    public int getItemCount() {
        return invitations.size();
    }

    @NonNull
    @Override
    public ActiveInvitationsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.request, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ActiveInvitationsAdapter.ViewHolder holder, int position) {
        Invitation invitation = invitations.get(position);
        holder.bind(invitation);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.requestText) TextView requestText;
        @BindView(R.id.requestDate) TextView requestTime;
        @BindView(R.id.requestSource) TextView requestSource;
        @BindView(R.id.requestUserImg) ImageView profileImage;
        @BindView(R.id.button2) Button validButton;
        @BindView(R.id.button3) Button refuseButton;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Invitation invitation) {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
            String dateString = sdf.format(invitation.getCreationDate());

            User me = authenticationProvider.getCurrentUser();
            User other = invitation.getFrom().equals(me.getUid()) ? invitation.getToUser() : invitation.getFromUser();
            requestSource.setText(other.getName());
            requestTime.setText(dateString);
            itemView.setOnClickListener(view -> listener.onInvitationClick(invitation));
            validButton.setOnClickListener(view -> listener.onInvitationAcceptClick(invitation));
            refuseButton.setOnClickListener(view -> listener.onInvitationRefuseClick(invitation));
        }
    }

    public interface Listener {
        void onInvitationAcceptClick(Invitation invitation);
        void onInvitationRefuseClick(Invitation invitation);
        void onInvitationClick(Invitation invitation);
    }
}
