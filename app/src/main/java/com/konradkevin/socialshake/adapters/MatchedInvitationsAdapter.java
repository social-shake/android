package com.konradkevin.socialshake.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.konradkevin.socialshake.R;
import com.konradkevin.socialshake.models.Invitation;
import com.konradkevin.socialshake.models.User;
import com.konradkevin.socialshake.providers.AuthenticationProvider;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MatchedInvitationsAdapter extends RecyclerView.Adapter<MatchedInvitationsAdapter.ViewHolder> {
    private List<Invitation> invitations;
    private AuthenticationProvider authenticationProvider;
    private Listener listener;

    public MatchedInvitationsAdapter(List<Invitation> invitations) {
        this.authenticationProvider = AuthenticationProvider.getInstance();
        this.invitations = invitations;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }


    @Override
    public int getItemCount() {
        return invitations.size();
    }

    @NonNull
    @Override
    public MatchedInvitationsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.match, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MatchedInvitationsAdapter.ViewHolder holder, int position) {
        Invitation invitation = invitations.get(position);
        holder.bind(invitation);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.matchName) TextView matchName;
        @BindView(R.id.MatchEmail) TextView matchEmail;
        @BindView(R.id.matchDate) TextView matchDate;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Invitation invitation) {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
            String dateString = sdf.format(invitation.getCreationDate());

            User me = authenticationProvider.getCurrentUser();
            User other = invitation.getFromUser().getUid().equals(me.getUid()) ? invitation.getToUser() : invitation.getFromUser();

            matchName.setText(other.getName());
            matchEmail.setText(other.getEmail());
            matchDate.setText(dateString);

            itemView.setOnClickListener(view -> listener.onInvitationClick(invitation));
        }
    }

    public interface Listener {
        void onInvitationClick(Invitation invitation);
    }
}
